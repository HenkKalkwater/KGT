KGT (Kurisu's GUI Toolkit) is a GUI toolkit (and more) written in C++ for the Nintendo 3DS.

This project is far from complete and should not be used.

# Goals
* GUI toolkit
    * Basic widgets
        * TextWidget for displaying text
        * ImageWidget for displaying images
        * ButtonWidget
            * TextButtonWidget: a button with text
            * ImageButtonWidget: a Button with an image
    
* 2D drawing library
    * Canvas: a class to draw on
        * Draw line
        * Draw image
        * Draw filled circle
        * Draw filled rectangle
        * Draw circle with outline
        * Draw rectangle with outline
        * Supports transformation, with TransformationStack
        * Draw font
* Mathematics:
    * Transformation: represents transformation
    * Vector2: represents an 2D vector
    * TransformationStack: a stack of transformations
* Sounds
    * WAV support
    * OGG support
    * BCSTM support
