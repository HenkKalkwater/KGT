/**
 * Header file that includes all files inside this project
 */
#ifndef KGT_H
#define KGT_H

//GRAPHICS
#include "graphics/Canvas.h"
#include "graphics/Image.h"

//GRAPHICS/UI
#include "graphics/ui/Action.h"
#include "graphics/ui/Container.h"
#include "graphics/ui/Divider.h"
#include "graphics/ui/Event.h"
#include "graphics/ui/ImageWidget.h"
#include "graphics/ui/Square.h"
#include "graphics/ui/UI.h"
#include "graphics/ui/VerticalContainer.h"
#include "graphics/ui/Widget.h"

//MATH
#include "math/Interpolation.h"
#include "math/Transformation.h"

//SOUND
#include "sound/WavSound.h"

//UTILS
#include "utils/Logger.h"
#include "utils/Thread.h"

#endif // KGT_H
