#ifndef IMAGEWIDGET_H
#define IMAGEWIDGET_H

#include "Widget.h"
#include "../Canvas.h"
#include "../Image.h"
#include "../../utils/Logger.h"
#include <string>

namespace KGT {

	enum ScaleType {
		ORIGINAL, //Don't scale at all
		STRETCH, //Scale as much as possible without keeping the aspect ratio
		COVER, //Scale as much as possible even if this may result in making a part of the image invisible and keep the aspect ratio, 
		FIT //Scale as much as possible and keep the aspect ratio, don't cover any part of the image 
	};

	class ImageWidget : public Widget {
		public:
			ImageWidget(Image* image = NULL, ScaleType scaleType = ORIGINAL) {
				this->image = image;
				this->scaleType = scaleType;
			}
			virtual void draw(Canvas* canvas);
			virtual void onSizeChanged();

			Image* getImage() { return image; }
			void setImage(Image* image) { this->image = image; }
			
		private:
			ScaleType scaleType;
			Image* image;

			//Variables used when the size is changed to position the image
			int calculatedX, calculatedY;
			float calculatedScaleX, calculatedScaleY;
	};

}

#endif //IMAGEWIDGET_H