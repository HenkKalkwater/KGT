#ifndef UI_H
#define UI_H

#include <3ds.h>
#include "Container.h"
#include "../Canvas.h"
#include "../../utils/Logger.h"

extern "C" {
    #include <sf2d.h>
}

namespace KGT {
	class UI {
		public:
            UI() {
                Log::debug("Got to constructor");
				topCanvas.setTransformationEnabled(true);
                Log::debug("Transformation enabled");
				topCanvas.getTransformation()->setPivot(TOP_WIDTH / 2, TOP_HEIGHT / 2);
                Log::debug("Transformation set");
			}

			static const int TOP_WIDTH = 400;
            static const int TOP_HEIGHT = 240;
            static const int BOTTOM_WIDTH = 320;
            static const int BOTTOM_HEIGHT = 240;

            void draw();
            void update(float delta);

            //Getters and setters

            void set3DEnabled(bool enabled) {
                threeDEnabled = enabled;
                sf2d_set_3D((int) enabled);
            }

            bool is3DEnabled() {
                return threeDEnabled;
            }

            /**
            * Sets the root widget of the top UI.
            * \param container to use as root widget
            */
            void setTopContainer(Container* container) {
                topContainer = container;
                topContainer->setWidth(UI::TOP_WIDTH);
                topContainer->setHeight(UI::TOP_HEIGHT);
                topContainer->invalidate();
            }

            /**
            * Sets the root widget of the bottom UI.
            * \param container to use as root widget
            */
			void setBottomContainer(Container* container) {
				bottomContainer = container;
			}



		private:
			bool threeDEnabled = true;
			Container* topContainer = NULL;
			Container* bottomContainer = NULL;
			Canvas topCanvas = Canvas(GFX_TOP);;
			Canvas bottomCanvas = Canvas(GFX_BOTTOM);;
    };


}

#endif //UI_H
