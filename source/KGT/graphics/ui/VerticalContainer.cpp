#include "VerticalContainer.h"

namespace KGT{
	void VerticalContainer::layout(){
		Log::debug("Laying out VerticalContainer");
		int addedHeight = 0;
		for(auto &i : children){
			i->setWidth(this->width - marginRight - marginLeft);
			i->setX(0 + marginLeft);
			i->setY(addedHeight + marginBottom);
			Log::debug("VerticalContainer: The position of the first child was set to: " + std::to_string(addedHeight));
			addedHeight += i->getHeight() + marginBottom + marginTop;
		}
		validate();
	}

	void VerticalContainer::update(float delta){
		Container::update(delta);
	}
}
