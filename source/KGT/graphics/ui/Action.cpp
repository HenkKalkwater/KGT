#include "Action.h"

namespace KGT{
    void Action::update(float delta) {
        
    }

    void Action::internalUpdate(float delta) {
		timePassed += delta;
        if (timePassed > duration + delay){
            finished = true;
            return;
        }

		if (timePassed > delay) {
			update(delta);
		}
    }
}
