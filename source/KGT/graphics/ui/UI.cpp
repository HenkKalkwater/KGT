#include "UI.h"
#include "../../utils/Logger.h"
#include <3ds.h>
extern "C" {
    #include <sf2d.h>
}
//KGT: Kwis Graphical Toolkit
//A little warning: I can't see in 3D, so the offset could be a little bit extreme
namespace KGT {

    /******************************
     * Draws the UI
     ******************************/
    void UI::draw() {
        if (topContainer) {
			topCanvas.start(GFX_LEFT);
			topContainer->applyTransformation(&topCanvas);
            topContainer->draw(&topCanvas);
			topContainer->removeTransformation(&topCanvas);
            topCanvas.stop();
            topCanvas.clearTransformation();

            if(threeDEnabled){
				topCanvas.start(GFX_RIGHT);
				topContainer->applyTransformation(&topCanvas);
				topContainer->draw(&topCanvas);
				topContainer->removeTransformation(&topCanvas);
				topCanvas.stop();
                topCanvas.clearTransformation();
            }
        }
        if (bottomContainer) {
			bottomCanvas.start(GFX_LEFT);
			bottomContainer->applyTransformation(&bottomCanvas);
            bottomContainer->draw(&bottomCanvas);
			bottomContainer->removeTransformation(&bottomCanvas);
            bottomCanvas.stop();
            bottomCanvas.clearTransformation();
        }
    }
    /******************************
     * Handles input events
     ******************************/
    void UI::update(float delta) {
		if (topContainer) {
			topContainer->update(delta);
		}

		if (bottomContainer) {
			bottomContainer->update(delta);
		}
    }

}
