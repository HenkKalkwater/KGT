#include "Canvas.h"

namespace KGT{
	void Canvas::start(gfx3dSide_t side) {
		currentSide = side;
        sf2d_start_frame(this->screen, currentSide);
        threeD = screen == GFX_TOP && CONFIG_3D_SLIDERSTATE > 0.0;
        depthSideMul = side == GFX_LEFT ? -1 : 1;
        depthSideMul *= CONFIG_3D_SLIDERSTATE;
	}

	void Canvas::stop() {
		sf2d_end_frame();
	}

    void Canvas::drawLine(int x1, int y1, int x2, int y2, int z) {
		if (transformEnabled) {
			transformationStack.transform(&x1, &y1);
			transformationStack.transform(&x2, &y2);
			if (threeD) {
				sf2d_draw_line(x1 + Z_MUL, y1, x2 + Z_MUL, y2, this->lineWidth, this->color);
			}else {
				sf2d_draw_line(x1, y1, x2, y2, this->lineWidth, this->color);
			}
		} else {
			if (threeD) {
				sf2d_draw_line(x1 + Z_MUL, y1, x2 + Z_MUL, y2, this->lineWidth, this->color);
			} else {
				sf2d_draw_line(x1, y1, x2, y2, this->lineWidth, this->color);
			}
		}
    }

    void Canvas::drawImage(Image* image, int x, int y, int z, bool blend) {
        if (transformEnabled){
			transformationStack.transform(&x, &y);
		}
		if (blend) {
			sf2d_draw_texture_rotate_hotspot_blend(image->getTexture(), x + Z_MUL, y, transformationStack.back()->getRotation(), 0.0, 0.0, this->color);
		} else {
			sf2d_draw_texture_rotate(image->getTexture(), x + image->getWidth() / 2 + Z_MUL, y + image->getHeight() / 2, transformationStack.back()->getRotation());
		}
    }

	void Canvas::enableClipping(int x, int y, int width, int height) {
		if (transformEnabled) {
			transformationStack.transform(&x, &y);
		}
		sf2d_set_scissor_test(GPU_SCISSOR_NORMAL, x, y, width, height);
	}

	void Canvas::disableClipping() {
		sf2d_set_scissor_test(GPU_SCISSOR_DISABLE, 0, 0, 0, 0);
	}
}
