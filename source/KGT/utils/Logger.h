#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include <iostream>

/**
 * A namespace used for logging. May implement logging to a file in the future.
 */
namespace Log {
	const static std::string mDebug = "\x1b[0m[\x1b[32m DEBUG \x1b[0m] ";
	const static std::string mInfo = "\x1b[0m[\x1b[34m INFO  \x1b[0m] ";
	const static std::string mWarn = "\x1b[0m[\x1b[33m WARN  \x1b[0m] ";
	const static std::string mError = "\x1b[0m[\x1b[31m ERROR \x1b[0m] ";


	const static int LEVEL_ERROR = 0;
	const static int LEVEL_WARN = 1;
	const static int LEVEL_INFO = 2;
	const static int LEVEL_DEBUG = 3;
	static int logLevel = LEVEL_DEBUG;

	void debug(std::string message);
	void info(std::string message);
	void warn(std::string message);
	void error(std::string message);

	void setLogLevel(int logLevel);

}
#endif // LOGGER_H
