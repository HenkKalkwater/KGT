#ifndef THREAD_H
#define THREAD_H

#include <3ds.h>
#include <3ds/types.h>
#include "Logger.h"

#define STACKSIZE (4 * 1024)

namespace KGT {

	class Thread {
		public:
			Thread(ThreadFunc threadFunction,  void* argument = NULL, size_t stackSize = STACKSIZE, int priority =  -1, int affinity = -2, bool detached = false) {
				numThreads += 1;
                if(priority < 0) {
					this->priority = getMainThreadPriority() - numThreads;
                }else {
					this->priority = priority;
                }
                this->detached = detached;
                this->thread = threadCreate(threadFunction, argument, stackSize, this->priority, affinity, this->detached);

				if(this->thread == NULL) {
					Log::error("Error creating thread.");
				}
			}
			~Thread(){
                if(!detached)
					threadFree(this->thread);
			}

			void join(u64 timeout = U64_MAX);

			s32 getId() { return this->id; }
			s32 getPriority() {return this->priority; }
			/**
			 * Gets the priority of the main thread. Can only be used on the main thread. (Should be fixed)
			 */
			static s32 getMainThreadPriority();
		private:
			s32 id;
			s32 priority;
			::Thread thread;
			bool detached;
			/**
			 * The amount of threads active
			 */
			static int numThreads;
	};

}


#endif // THREAD_H
