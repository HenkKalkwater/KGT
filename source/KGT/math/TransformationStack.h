#ifndef TRANSFORMATIONSTACK_H
#define TRANSFORMATIONSTACK_H

#include "Transformation.h"
#include <vector>

namespace KGT {

	//Maybe it's a better idea to extend Vector, but I'm not sure. 
	class TransformationStack {
		public:
            TransformationStack(){
                Transformation tmp = Transformation();
                push(tmp);
            }
			/**
			 * Pops the latest transformation of the stack
			 * \return The new stack size
			 */
			size_t pop();
			/**
			 * Pushes the transformation on the stack
			 * \return The new stack size
			 */ 
			size_t push(Transformation transformation);

			/**
			 * Resets the transformation stack
			 */ 
			void reset();

			Transformation* back() { return &stack.back(); }

			void transform(int* x, int* y);
			void transform(float* x, float* y);
		private:
			std::vector<Transformation> stack;
	};

}

#endif //TRANSFORMATIONSTACK_H
