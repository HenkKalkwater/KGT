#ifndef BUILD_H
#define BUILD_H

enum BuildType = {
    CIA, THREEDSX
}

const static BuildType BUILD_TYPE = THREEDSX;

#endif // BUILD_H
